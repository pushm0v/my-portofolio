---
type: "Work Experience"
heading: "GO-JEK"
subheading: "Scrum Master, Tech Lead"
duration: "June 2015 - May 2016"
location: "Jakarta, Indonesia"
---

Work closely with Product Owner to deliver new product based on Scrum Process.

* iOS, Swift, Java Spring, ElasticSearch, MySQL